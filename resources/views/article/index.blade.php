@extends('app')
<title>UZ | Moji Postovi</title>
@section('content')
<div class="telo" ng-app="ajaxApp" ng-controller="AjaxAngularController as angCtrl">
<img src="{{ asset('/images/mozak.gif') }}" alt="Slika" class="slika">
<h3><i class="fa fa-users"></i> Teme </h3> 
    <div class="popoverDiv container" style="padding-top:0.5%" ng-if="angCtrl.popoverIsVisible"><p>Hint: Dvoklikom na naslov ili deskripciju mozete promeniti iste!</p></div> <br>
       <table class="table table-responsive ellipsisTable"  id="htmlTable">

            <thead>                
                <tr>   
                    <th>Korisnik</th>
                    <th>Naslov</th>
                    <th>Deskripcija</th>
                    <th>Email</th>
                    <th>Date/Time Added/Updated</th>

                </tr>
            </thead>    
                @foreach($articles as $article)
                    <div class="col-md-12" >
                            <tr data-id-itema = "{{ $article->id }}" class="js-item-row">
                                <td class="info">
                                    {{ $article->user->name }}
                                </td>
                                <td class="js-title" ng-mouseover="angCtrl.showPopover()" ng-mouseleave="angCtrl.hidePopover()">
                                    {{ $article->title }}
                                </td>
                                <td class="active js-body ellipsisTd" ng-mouseover="angCtrl.showPopover()" ng-mouseleave="angCtrl.hidePopover()" >
                                    {{ $article->body }}
                                </td>
                                <td class="active ">
                                    {{ $article->user->email }}
                                </td>
                                <td class="js-time">
                                    {{ $article->updated_at}}
                                </td>
                                <!-- <td class="active">
                                    {!! Form::open(['route' => ['article.edit', $article->id], 'method' => 'GET' ]) !!}
                                    {!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
                                    {!! Form::close() !!}
                                    
                                </td>
                                <td class="active">                        
                                    {!! Form::open(['route' => ['article.destroy', $article->id], 'method' => 'DELETE']) !!}
                                    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td> -->
                                <td class="btn-save"  data-id-itema = "{{ $article->id }}">
                                </td>
                                <td class="btn-cancel">
                                </td>
                                <td>
                                    <button class="btn btn-danger js-obrisi" data-id-itema="{{ $article->id }}"><span class="glyphicon glyphicon-trash"></span></button>
                                </td>
                            </tr>
                        
                    </div>
                @endforeach    
        </table>
        <button id="addnew" class="btn btn-success">Dodaj novi post</button>
        <div id="inputi"></div>
        
    </div> 

    


@endsection


@section('scripts')
    <script src="{{ asset('/js/main.js') }}"></script> 
    <script src="{{ asset('/js/ajax-angular.js') }}"></script>
    <script type="text/javascript">
        window._laravel_token = "{{{ csrf_token() }}}";
        window._laravel_user = {!! $user->toJson() !!};
    </script>   
@endsection