@extends('app')
<title>UZ | Forum</title>
@section('content')

<div class="telo formMargin">
<img src="{{ asset('/images/forum.gif') }}" alt="Slika" class="slika">
		<div ng-app="ajaxApp">
				<div ng-controller="AjaxAngularController as angCtrl">
					<div ng-if="!angCtrl.hideTable">
						<h2>Forum</h2>
						<br>
							Pretrazi teme:&nbsp;&nbsp;<input type="text" ng-model="text"/>
							<br><br><br>
							<table class="table table-responsive ellipsisTable" id="htmlTable">
					            <thead>
					                 <tr>
					                 	<th>Temu pokrenuo</th>
					                    <th>Naziv teme</th>
					                    <th class="hidden-xs">Poslednji post</th>
					                </tr>
					            </thead>
				                    <div class="col-md-12">
				                    @if(Auth::user()->is('admin'))
				                    <div class="col-md-12">
			                            <tr ng-repeat="article in angCtrl.articles | filter:angCtrl.search | filter:text" ng-dblclick="angCtrl.editArticle(article)">
			                            	<td>@{{ article.user_name }}</td>
			                            	<td><span ng-if="!article.editMode">@{{ article.title }}</span><input type="text" class="inputEditField" ng-if="article.editMode" ng-model="article.title"></td>
			                            	<td class="hidden-xs">@{{ article.updated_at }}</td>
			                            	<td><button ng-if="article.editMode" class="btn btn-info" ng-click="angCtrl.saveArticle(article)"><span class="glyphicon glyphicon-ok"></span></button><td>
			                            	<td><button ng-if="article.editMode" class="btn btn-danger" ng-click="angCtrl.cancel(article)"><span class="glyphicon glyphicon-remove"></span></button><td>
			                            	<td><button ng-if="!article.editMode" class="btn btn-info" ng-click="angCtrl.showPost(article)"><span class="glyphicon glyphicon-search"></span></button></td>
			                            	<td><button ng-if="!article.editMode" class="btn btn-danger" ng-click="angCtrl.delete(article)"><span class="glyphicon glyphicon-trash"></span></button></td>
			                        	</tr>
			                        @else
				                        <tr ng-repeat="article in angCtrl.articles | filter:angCtrl.search | filter:text">
			                            	<td>@{{ article.user_name }}</td>
			                            	<td>@{{ article.title }}</td>
			                            	<td>@{{ article.updated_at }}</td>
			                            	<td><button class="btn btn-primary" ng-click="angCtrl.showPost(article)"><span class="glyphicon glyphicon-search"></span></button></td>
				                        </tr>
			                        @endif
				                    </div>   
				        </table>
				        @if (!Auth::guest())
				        <button ng-click="angCtrl.show()" class="btn btn-success">Dodaj temu</button>
				        @endif
				        <div ng-if="angCtrl.showAddForm">
					       <form ng-submit="angCtrl.sendPost()">
						       <div class="form-group col-md-3">
							       <label> Naslov: </label>
							       <input type="text" class="form-control" ng-model="angCtrl.newArticle.title" required>
							       <label> Telo: </label>
							       <textarea class="form-control" rows="4" cols="50" ng-model="angCtrl.newArticle.body" required ></textarea><br>
							       <button class="btn btn-info form" type="submit"><span class="glyphicon glyphicon-ok"></span></button>
							       <button class="btn btn-danger form" type="reset" ><span class="glyphicon glyphicon-remove"></span></button><br><br>
							       <button ng-click="angCtrl.hide()" class="btn btn-default">Sakrij</button>
						       </div>
					       </form>
				        </div>
				    </div>
				    <div  ng-if="angCtrl.hideTable">
			        	<h2>@{{ angCtrl.articles.title }}</h2>
			        	<p>Temu pokrenuo: @{{ angCtrl.articles.user_name }}</p><p>Poslednji post: @{{ angCtrl.articles.updated_at }}</p>
			        	<p>@{{ angCtrl.articles.body }}</p><hr>
			        	<div class="commentDiv">
	                       @if(Auth::user()->is('admin'))
				        	<ul class="list-unstyled" ng-repeat="comment in angCtrl.comments | filter : { forum_id: angCtrl.articles.id} " ng-dblclick="angCtrl.editComment(comment)">
	                        	<li>Komentarisao: @{{ comment.user_name }}</li><br>
	                        	<li>Vreme: @{{ comment.updated_at }}</li><br>
	                        	<li>Komentar: &nbsp;<span ng-if="!comment.editMode">@{{ comment.body }}</span><textarea ng-if="comment.editMode" ng-model="comment.body"></textarea>
                            	<p><button ng-if="comment.editMode" class="btn btn-primary" ng-click="angCtrl.saveComment(comment)"><span class="glyphicon glyphicon-ok"></span></button></p>
                            	<button ng-if="comment.editMode" class="btn btn-default" ng-click="angCtrl.cancelCommentEdit(comment)"><span class="glyphicon glyphicon-remove"></span></button><li><br>
	                        	<li>&nbsp;<button ng-if="!comment.editMode" class="btn btn-danger" ng-click="angCtrl.deleteComment(comment)"><span class="glyphicon glyphicon-trash"></span></button></li><hr>
	                        </ul>
	                       @else
	                       	<ul class="list-unstyled" ng-repeat="comment in angCtrl.comments | filter : { forum_id: angCtrl.articles.id} ">
	                       		<li>Komentarisao: @{{ comment.user_name }}</li><br>
	                       		<li>Vreme: @{{ comment.updated_at }}</li><br>
	                        	<li>Komentar: @{{ comment.body }}</li><br><hr>
	                       	</ul>
	                       @endif
	                    </div>
			        	<div ng-if="angCtrl.showRepForm">	
			        		<form ng-submit="angCtrl.addComment()">
				        		<textarea class="form-control" rows="6" ng-model="angCtrl.newComment.body" required></textarea><br>
				        		<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
				        		<button  ng-click="angCtrl.commentHide()" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
			        		</form>
			        	</div>
			        	<button ng-click="angCtrl.commentShow()" ng-if="!angCtrl.showRepForm" class="btn btn-info">Replika</button>
			        	<a class="btn btn-default" href="{{ url('/forum') }}">Nazad</a>
			        </div>
		        </div>
		</div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/ajax-angular.js') }}"></script>
    <script type="text/javascript">
        window._laravel_token = "{{{ csrf_token() }}}";
    </script>  
@endsection