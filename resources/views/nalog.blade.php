@extends('app')
<title>UZ | Moji nalog</title>
@section('content')
<div class="telo formMargin" ng-app="ajaxApp" ng-controller="AjaxAngularController as angCtrl">
<h3><i class="fa fa-users"></i> Moj nalog </h3> 
       <table class="table table-responsive ellipsisTable" id="htmlTable">

            <thead>                
                <tr>   
                    <th>Ime korisnika</th>
                    <th>Email</th>
                    <th>Datum kreiranja</th>
                </tr>
            </thead>  
                <tr>
                    <td>{{ Auth::user()->name }}</td>
                    <td>{{ Auth::user()->email }}</td>
                    <td>{{ Auth::user()->created_at }}</td>
                </tr>  
        </table>        <div >
            <form ng-submit="angCtrl.addVesti()">
                <input type="text" class="form-control" ng-model="angCtrl.newVesti.title" placeholder="Unesite naslov ovde..." required><br>
                <textarea class="form-control" rows="6" ng-model="angCtrl.newVesti.body" placeholder="Unesite tekst vesti ovde..." required></textarea><br>
                <button type="submit" class="btn btn-success">Dodaj vest</button>
            </form>
        </div>  

            <div ng-if="!angCtrl.hideVestiTable">
                <ul class="list-unstyled" ng-repeat="vest in angCtrl.vesti"> 
                        <li><h3>@{{ vest.title }}</h3></li>
                        <li class="ellipsisLi">@{{ vest.body }}</li><br>
                        <li><a class="btn btn-default" ng-click="angCtrl.showVest(vest)">Procitaj vise</a></li>
                        <br>
                        <li><a class="btn btn-danger" ng-click="angCtrl.deleteVest(vest)"><span class="glyphicon glyphicon-trash"></a></li>
                        <hr>
                </ul>
            </div>
            <div ng-if="angCtrl.hideVestiTable">
                <p><h3>@{{ angCtrl.vest.title }}</h3></p>
                <p>@{{ angCtrl.vest.body }}</p>
                <p><a href="/nalog" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-arrow-left"></span></a></p>
            </div>
</div> 

    


@endsection


@section('scripts')
    <script src="{{ asset('/js/ajax-angular.js') }}"></script>
    <script type="text/javascript">
        window._laravel_token = "{{{ csrf_token() }}}";
    </script>  
@endsection