<!DOCTYPE html>
<html lang="en">
<head>
	<title>Upravljanje znanjem</title>
	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name='description' content='Sajt Udruženje građana Inicijativa za upravljanje znanjem Novi Sad, Srbija, koje se bavi  upravljanjem znanjem, primenom upravljanja znanjem 
   			i obukom za upravljanje znanjem -knowledge management' />
   		<meta name='keywords' content='upravljanje znanjem, inicijativa za upravljanje znanjem, knowledge management,  internet programiranje, 
			web programiranje, seminari, kursevi, kursevi iz računara, kursevi informatike, seminari iz menadžmenta, seminari iz upravljanja znanjem,
			obuke, obuke za menadžment, obuke za računare,  Tarzan,  upravljanje znanjem Novi Sad, upravljanje znanjem u Srbiji, razvoj web poslovnih aplikacija' />

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	@yield('styles')

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body data-spy="scroll" data-target="#mainNav" style="margin-bottom:8%">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="{{ set_active('/') }}"><a href="/">Početna</a></li>
					<li class="{{ set_active('udruzenje') }}"><a href="{{ url('/udruzenje') }}">Udruženje</a></li>
					@if(Auth::guest())
					<li class="{{ set_active('forum') }}"><a data-toggle="modal" data-target="#login-modal" href="">Forum</a></li>
					@else
					<li class="{{ set_active('forum') }}"><a href="{{ url('/forum') }}">Forum</a></li>
					@endif
					<li class="{{ set_active('seminari') }}"><a href="{{ url('/seminari') }}">Seminari i obuke</a></li>
					<li class="{{ set_active('buducnost') }}"><a href="{{ url('/buducnost') }}">Prozor u budućnost</a></li>
					<li class="{{ set_active('kontakt') }}"><a href="{{ url('/kontakt') }}">Kontakt</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a data-toggle="modal" data-target="#login-modal" href="">Uloguj se</a></li>
						<li><a data-toggle="modal" data-target="#register-modal" href="">Registracija</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/article') }}">Moji postovi</a></li>
								<li><a href="{{ url('/indexCom') }}">Moji komentari</a></li>
								@if(Auth::user()->is('admin'))
								<li><a href="{{ url('/nalog') }}">Moji nalog</a></li>
								@endif
								<li role="separator" class="divider"></li>
								<li><a href="{{ url('/auth/logout') }}">Izloguj se</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
	<div class="glavni">
		<div class="container">
	    	@yield('content')
	  	</div>
	</div>
  @if(!Auth::check())
    @include('modals.register')
    @include('modals.login')
    @include('modals.password')
  @endif

  @yield('modals')



	<!-- Scripts -->

	<script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('/vendor/angular/angular.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/app.js') }}"></script>
    <link href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--<script src="{{ asset('/js/app.js') }}"></script>-->
	@yield('scripts')


</body>
	 <footer class="hidden-xs">
	    <div class="navbar navbar-fixed-bottom navbar-default">
	        <div class="container" style="margin-top:1%">
			  <div class="span12 row" style="text-align: center">
			    <p>Upravljanje znanjem | Novi Sad</p>
			  </div>
			</div>
	    </div>
	</footer>
</html>
