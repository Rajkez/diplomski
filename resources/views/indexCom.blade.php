@extends('app')
<title>UZ | Moji Komentari</title>
@section('content')
<div class="telo" ng-app="ajaxApp" ng-controller="AjaxAngularController as angCtrl">
<img src="{{ asset('/images/mozak.gif') }}" alt="Slika" class="slika">
<h3><i class="fa fa-users"></i> Teme </h3> 
       <table class="table table-responsive ellipsisTable" id="htmlTable">
            <thead>                
                <tr>   
                    <th>Ime teme</th>
                    <th>Komentar</th>
                    <th>Date/Time Added/Updated</th>

                </tr>
            </thead>    
                @foreach($comments as $comment)
                    <div class="col-md-12" >
                            <tr data-id-itema = "{{ $comment->id }}" class="js-item-row">
                                <td class="info">
                                    {{ $comment->forum_title }} 
                                </td>
                                <td class="active js-body ellipsisTd" ng-mouseover="angCtrl.showPopover()" ng-mouseleave="angCtrl.hidePopover()" >
                                    {{ $comment->body }}
                                </td>
                                <td class="js-time">
                                    {{ $comment->updated_at}}
                                </td>
                                <td class="btn-save"  data-id-itema = "{{ $comment->id }}">
                                </td>
                                <td class="btn-cancel">
                                </td>
                                <td>
                                    <button class="btn btn-danger js-obrisi" data-id-itema="{{ $comment->id }}"><span class="glyphicon glyphicon-trash"></span></button>
                                </td>
                            </tr>
                    </div>
                @endforeach    
        </table>


    </div> 

    


@endsection


@section('scripts')
    <script src="{{ asset('/js/main.js') }}"></script> 
    <script src="{{ asset('/js/ajax-angular.js') }}"></script>
    <script type="text/javascript">
        window._laravel_token = "{{{ csrf_token() }}}";
        window._laravel_user = {!! $user->toJson() !!};
    </script>   
@endsection