@extends('app')
<title>UZ | Udruženje</title>
@section('content')

<!DOCTYPE html>
    <div class="telo box">
    <img src="{{ asset('/images/mozak.gif') }}" alt="Slika" class="slika">
    	<p><b>Naziv udruženja : Udruženje građana "Inicijativa za upravljanje znanjem"<br>Sedište udruženja: Lazara Stejina 7, 21241 Kać.<br>Udruženje svoju delatnost ostvaruje na teritoriji Republike Srbije.</b></p>
		<p>Udruženje građana Inicijativa za upravljanje znanjem" osnovano je kao nestranačko, nevladino i neprofitno udruženje građana čiji je cilj naučno i stručno istraživanje oblasti Upravljanja znanjem (Knowledge Management - KM), obrazovanje vlasnika, menadžerske strukture i zaposlenih u preduzećima i drugim organizacijama za primenu metoda upravljanja znanjem u poslovanju i razvijanje svesti o neophodnosti primene inicijativa za upravljanje znanjem u poslovanju i prelaska iz industrijskog u "doba znanja". Na ovaj način Udruženje će dati doprinos prestruktuiranju naših privrednih subjekata koje je neophodno da se ostvari globalna konkurentnost naših preduzeća i drugih organizacija i time ukupan oporavak društva i ostvarenje društvenog prosperiteta odnosno bolji život svih članova društva.</p>
		<p>Radi ostvarivanja svojih ciljeva, Udruženje naročito:<br>1. prikuplja i obrađuje naučnu i stručnu literaturu u oblasti upravljanja znanjem (Knowledge Management);<br>2. organizuje, samo ili u zajednici sa drugim organizacijama, stručne skupove, savetovanja, seminare i druge oblike stručnog obrazovanja u ovoj oblasti;<br>3. objavljuje knjige i druge publikacije o upravljanju znanjem, u skladu sa zakonom;<br>4. organizuje naučne i stručne radnike za rad na naučnim, stručnim i istraživačkim projektima iz domena upravljanja znanjem<br>5. sarađuje sa univerzitetima, stručnim udruženjima i privrednim organizacijama u zemlji i inostranstvu koje se bave upravljanjem znanjem, a posebno primenom metoda upravljanja znanjem (Knowledge Management) u poslovnim organizacijama.</p>
		<p>Upravljanje znanjem pomaže organizaciji da obezbedi efektivnost (raditi prave stvari) i efikasnost (raditi na pravi način) u svom poslovanju, sa ciljem dramatičnog poboljšanja savremenih merila poslovanja: -stepena zadovoljstva kupca, sposobnost da se proizvodi za poznatog kupca, u svemu prema njegovim potrebama, zahtevima, željama i mogućnostima, cena, rokovi, kvalitet, stepen zadovoljstva zaposlenih, odgovornost prema životnoj sredini i društvenoj zajednici.</p>
		<p>Pozivamo Vas da se javite, sa predlozima za saradnju, idejama, komentarima i vašim iskustvima. Izgradimo zajednički mrežu znanja. Krajnje je vreme da i mi naučimo kako se posluje u <b>"dobu znanja"</b>. Može li <b>"Zastava"</b> postati <b>"Toyota"</b>? Naš odgovor je ne samo "može", nego i <b>MORA</b>, ako želimo šansu da i za nas postoji budućnost!</p>
    </div>



@endsection