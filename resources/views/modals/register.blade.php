<div id="register-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <strong>Regstracija</strong> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><br>
      </div>

        <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel-body">
                <form id="signup-form" class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group">
                    <label class="col-md-4 control-label">Ime</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label">E-Mail Adresa</label>
                    <div class="col-md-6">
                      <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                      <span class="help-inline text-danger"></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label">Lozinka</label>
                    <div class="col-md-6">
                      <input type="password" class="form-control" name="password">
                      <span class="help-inline text-danger"></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label">Potvrdi lozinku</label>
                    <div class="col-md-6">
                      <input type="password" class="form-control" name="password_confirmation">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                        Registruj se
                      </button>
                      <br><br>
                      <p>Već imate korisnički nalog?&nbsp; <a data-toggle="modal" data-dismiss="modal" data-target="#login-modal" href="">Uloguj se</a></p>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
