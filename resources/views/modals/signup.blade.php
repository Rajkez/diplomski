<div id="sign-up-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
            <h2>sign up</h2>

            <form id="signup-form" role="form" method="POST" action="{{ url('/auth/register') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="username" name="username">
                <span class="help-inline text-danger"></span>
              </div>

              <div class="form-group">
                <input type="password" class="form-control" placeholder="password" name="password">
                <span class="help-inline text-danger"></span>
              </div>

              <div class="form-group">
                <input type="password" class="form-control" placeholder="confirm password" name="password_confirmation">
                <span class="help-inline text-danger"></span>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block" data-loading-text="Loading...">sign up</button>
                <span class="help-block">
                  already joined?
                  <a data-dismiss="modal" data-toggle="modal" data-target="#login-modal" href="">log in</a>
                </span>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
