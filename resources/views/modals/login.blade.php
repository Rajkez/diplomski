<div id="login-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <strong>Logovanje</strong> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><br>
      </div>
       <div class="modal-body">
       <div class="form-errors alert alert-danger hidden" style="margin:16px 32px;"></div>
        <form id="singup-form" class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
              <label class="col-md-4 control-label">E-Mail Adresa</label>
              <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                <span class="help-inline text-danger"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Lozinka</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password" required>
                <span class="help-inline text-danger"></span>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember"> Zapamti me
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Logovanje</button>
                &nbsp;&nbsp;&nbsp;
                <a data-toggle="modal" data-target="#password-modal" data-dismiss="modal" href="">Zaboravljena lozinka?</a>
                <br><br>
                <p>Nemate korisnicki nalog?&nbsp; <a data-toggle="modal" data-dismiss="modal" data-target="#register-modal" href="">Registracija</a></p>   
              </div>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>
