@extends('app')
<title>Upravljanje znanjem</title>
@section('content')
<!DOCTYPE html>
<div ng-app="ajaxApp" ng-controller="AjaxAngularController as angCtrl">
	<div class="telo box">
	<img src="{{ asset('/images/mozak.gif') }}" alt="Slika" class="slika">
		<div class="stranica">
	            Vesti
	        </div>
	        <div ng-if="!angCtrl.hideVestiTable">
	            <ul class="list-unstyled" ng-repeat="vest in angCtrl.vesti"> 
	               		<li><h3>@{{ vest.title }}</h3></li>
	                	<li class="ellipsisLi">@{{ vest.body }}</li><br>
	                	<li><a class="btn btn-default" ng-click="angCtrl.showVest(vest)">Procitaj vise</a></li>
	            </ul>
	        </div>
	        <div ng-if="angCtrl.hideVestiTable">
	        	<p><h3>@{{ angCtrl.vest.title }}</h3></p>
	        	<p>@{{ angCtrl.vest.body }}</p>
	        	<p><a href="/" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-arrow-left"></span></a></p>
	        </div>
	</div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('/js/ajax-angular.js') }}"></script>
    <script type="text/javascript">
        window._laravel_token = "{{{ csrf_token() }}}";
    </script>  
@endsection