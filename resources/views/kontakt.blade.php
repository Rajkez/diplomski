@extends('app')
<title>UZ | Kontakt</title>
@section('content')

<!DOCTYPE html>

<br>
<div class="box">
        <div class="stranica">
            Kontakt
        </div>
        <div class="telo">
            <p>e-mail: <a href="mailto:kontakt@uzns.net">kontakt@uzns.net</a></p>
            <p>Telefon: <strong>064/416-95-76</strong> (dr Zoran Lovreković) </p>
        </div>
    </div>
@endsection