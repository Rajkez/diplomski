$(document).ready(function() {
  
  var ajaxFromSubmit = function(e) {
    e.preventDefault();
    var $form = $(this);
    var method = $form.find('input[name="_method"]').val() ||  $form.attr('method');
    var url = $form.prop('action');
    var $submitButton = $form.find('button[type="submit"]');

    $form.find('.form-group .help-inline').addClass('hidden');
    $form.find('.form-group').removeClass('has-error');
    $form.parents('.modal-body').find('.form-errors').addClass('hidden');

    $.ajax({
      method: method,
      url: url,
      data: $form.serialize(),
      success: function(data, textStatus, jqXHR) {
        $form.find(':input').val('');
        if(jqXHR.status === 200 ) {
          $form.parents('.modal').modal('hide');
          $(location).prop( 'pathname', '/');     
        }
      },
      error: function(data) {
        if( data.status === 422 ) {
          var errors = data.responseJSON;
          var field;
          console.log(errors);
          $.each(errors, function(fieldName, message) {
            field = $form.find(':input[name='+ fieldName +']');
            field.next().text(message).removeClass('hidden');
            field.parents('.form-group').addClass('has-error');
          });
        } else {
          var errors = data.responseJSON.msg;
          $form.parents('.modal-body').find('.form-errors').text(errors).removeClass('hidden');
          console.log(errors);
        }
        $submitButton.prop('disabled', false);
      }
    });
  };

  var checkPostField = function(e) {
    var $form = $(this);
    var $field = $form.find('.new-post-text');
    if($field.length < 1) {
      $field = $form.find('.feedback');
    }
    var inputLength = $field.val().length;
    $form.find('.form-group .help-inline').addClass('hidden');
    $form.find('.form-group').removeClass('has-error');
    if(inputLength < 3 || (inputLength > 1000 && $field.hasClass('new-post-text'))) {
      e.preventDefault();
      $field.next().text('This field is required').removeClass('hidden');
      $field.parents('.form-group').addClass('has-error');
      if(inputLength === 0 ) {
        $field.next().text('This field is required').removeClass('hidden');
      } else if(inputLength < 3){
        $field.next().text('This field must be at least 3 characters long.').removeClass('hidden');
      } else {
        $field.next().text('This field may not be greater than 1000 characters.').removeClass('hidden');
      }
    }
  };

  $('#login-form').on('submit', ajaxFromSubmit);
  $('#signup-form').on('submit', ajaxFromSubmit);


  // if($('#welcome-modal').length) {
  //   $('#welcome-modal').modal();
  // }

  if($('.alert-dismissible').length) {
    window.setTimeout(function(){
      $('.alert-dismissible').remove();
    }, 2000);
  }

});
