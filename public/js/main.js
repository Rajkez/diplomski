(function(){
	$(document).on('ready', function(){
		
///////////////////////////////////////////////////////
/*                      AJAX                         */
///////////////////////////////////////////////////////

		$('button#callapi').on('click', function(event){

			$.ajax({

				url: 'http://test1.dev/ispis',

				success: function (data){
					console.log(data)

					var post1 = $('table tr td#polje-1');
					var post2 = $('table tr td#polje-2');

						post1.html('');
						post2.html('');

						$.each(data, function(index, post){
							post1.append('<tr><td>' + post.title + '</td></tr>');
							post2.append('<tr><td>' + post.body + '</td></tr>');
						})
					
					}
				});
			});

///////////////////////////////////////////////////////
/*                  AJAX UPDATE                      */
///////////////////////////////////////////////////////

		$('body').on('dblclick', '.js-item-row', function(evt){

			var $obj = $(this);
			//console.log($obj);
			var idItema = $obj.data("id-itema");
			var $opis = $obj.find('.js-body');
			var $naslov = $obj.find('.js-title');
			var $save = $obj.find('.btn-save');
			var $cancel = $obj.find('.btn-cancel');
			var $obrisi = $obj.find('.js-obrisi');
			var $time = $obj.find('.js-time')
			var tekstNaslova = $naslov.html().trim();
			var tekstOpisa = $opis.html().trim();
			$save.show();
			$cancel.show();
			//console.log(tekstNaslova);
			//alert(idItema);

			$naslov.html('<input type="text" size="20" data-id-itema="' + idItema + '" id="naslov-' + idItema + '" >');
			$opis.html('<textarea size="30" data-id-itema="' + idItema + '" id="opis-' + idItema + '" ></textarea>');
			$('#naslov-'+idItema).val(tekstNaslova);
			$('#opis-'+idItema).val(tekstOpisa);

			$save.html('<button data-id-itema="'+idItema+'" id="sacuvaj-'+idItema+'" class="btn btn-info"><span class="glyphicon glyphicon-ok"></span></button>');
			$cancel.html('<button data-id-itema="'+idItema+'" id="ponisti-'+idItema+'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>');		
			$obrisi.hide();

			$('#ponisti-'+idItema).on('click', function(){
				$obrisi.show();
				$save.hide();
				$cancel.hide();
				$naslov.html(tekstNaslova);
				//console.log($naslov);
				$opis.html(tekstOpisa);
			})

			$('#sacuvaj-'+idItema).on('click', function(){
				var $post = {};
				$post.title = $('#naslov-'+idItema).val();
				//console.log($post.title);
				$post.body = $('#opis-'+idItema).val();
				$post._token = window._laravel_token;
				var id = $(this).data('id-itema');
				$naslov.html($('#naslov-'+idItema).val());
				//console.log($naslov);
				$opis.html($('#opis-'+idItema).val());
				console.log($post);
				
				$.ajax({

					type:'PUT',
					url:'/ajax-articles/'+id,
					data: $post,
					success: function(data){
						$save.hide();
						$cancel.hide();
						$obrisi.show();
						$time.html(data.updated_at);

					}

					})

			})

		});

///////////////////////////////////////////////////////
/*                 AJAX ADD NEW                      */
///////////////////////////////////////////////////////

		$('button#addnew').on('click', function(){
			$('#inputi').html('<form><div class="form-group col-md-3"><label> Naslov: </label> <input type="text" id="naslov" class="form-control" required><label> Opis: </label> <textarea id="opis" class="form-control" required></textarea> <br> <button class="btn btn-info form" id="posalji" type="submit"><span class="glyphicon glyphicon-ok"></span></button> <button id="sakrij" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></div></form>');
				
				$('#sakrij').on('click', function(){
					$('#inputi').html('');
				})

				$('#posalji').on('click', function(){

						var $post = {};
						$post.title = $('#naslov').val();
						$post.body = $('#opis').val();
						$post.user_name = window._laravel_user.name;
						$post._token = window._laravel_token;
						//console.log($post);
					
					$.ajax({

						type: "POST",
						url: '/ajax-articles/ajax-store',
						data: $post,
						success: function(data){
							$('#inputi').html('');
							//console.log(data);
							var email = window._laravel_user.email;
							var username = window._laravel_user.name;

							var noviRed = $("<tr class='js-item-row' data-id-itema='"+data.id+"'></tr>");
							noviRed.append('<td class="info">'+username+'</td>');
							noviRed.append('<td class="js-title">'+data.title+'</td>');
							noviRed.append('<td class="active js-body">'+data.body+'</td>');
							noviRed.append('<td class="active">'+email+'</td>');
							noviRed.append('<td>'+data.created_at+'</td>');
							noviRed.append('<td class="btn-save"></td>');
							noviRed.append('<td class="btn-cancel"></td>');
							noviRed.append('<button class="js-obrisi btn btn-default" data-id-itema="'+data.id+'">Obrisi</button>');
							$('table').append(noviRed);
							// $('table tr td#title').append('<tr><td>'+data.title+'</tr></td>');
							// $('table tr td#body').append('<tr><td>'+data.body+'</tr></td>');
						}

					});
				});
		});

///////////////////////////////////////////////////////
/*                  AJAX DELETE                      */
///////////////////////////////////////////////////////

		$('body').on('click', '.js-obrisi', function(){
			var id = $(this).data('id-itema');
			var token = $(this).data(token);
			$.ajax({
				url:'/ajax-articles/'+id+'/delete',
				type: 'delete',
				data: {method: 'delete', _token:window._laravel_token},
    			success: function(data){
    				$("tr[data-id-itema=" + id + "]").hide();
    			}
			});
		})

	});
})();