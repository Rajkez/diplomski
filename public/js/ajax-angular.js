angular.module('ajaxApp', [])
	.controller('AjaxAngularController', function($http){
		var angCtrl = this;
		angCtrl.showTable = false;
		angCtrl.newArticle = {
			title: '',
			body: '',
			user_name: '',
			user_id: window._laravel_token,
			created_at: ''

		};
		angCtrl.newComment = {
			body: '',
			forum_id: '',
			forum_title: '',
			user_name: '',
			user_id: window._laravel_token,
			created_at: ''
		};

		angCtrl.showAddForm = false;
			$http.get('/ispis').
				success(function(data){
					angCtrl.articles = data;
				}).
				error(function(){

				});

			$http.get('/ispisCom').
				success(function(data){
					angCtrl.comments = data;
				}).
				error(function(){

				});

			$http.get('/ispisVesti').
				success(function(data){
					angCtrl.vesti = data;
				}).
				error(function(){

				});

			angCtrl.delete = function(data){
				angCtrl.index = angCtrl.articles.indexOf(data);
				$http.delete('/ajax-angular/'+data.id+'/delete', data).
					success(function(data){
						angCtrl.articles.splice(angCtrl.index, 1);
					});
			};

			angCtrl.deleteComment = function(data){
				angCtrl.index = angCtrl.comments.indexOf(data);
				$http.delete('/ajax-angular/angular-comment/'+data.id+'/delete', data).
					success(function(data){
						angCtrl.comments.splice(angCtrl.index, 1);
					});
			};

			angCtrl.deleteVest = function(data){
				angCtrl.index = angCtrl.vesti.indexOf(data);
				$http.delete('/ajax-angular/angular-vest/'+data.id+'/delete', data).
					success(function(data){
						angCtrl.vesti.splice(angCtrl.index, 1);
					});
			};

			angCtrl.show = function(){
				angCtrl.showAddForm = true;
			}
			angCtrl.hide = function(){
				angCtrl.showAddForm = false;
			}

			angCtrl.sendPost = function(){
				$http.post('/ajax-angular/angular-store', angCtrl.newArticle).
					success(function(data){
						angCtrl.articles.push(data);
						angCtrl.showAddForm = false;
						angCtrl.newArticle = {
							title: '',
							body: '',
							user_name: '',
							user_id: window._laravel_token,
							created_at: ''
						};
					});
			}

			angCtrl.showPost = function(article){
				angCtrl.hideTable = true;
				$http.get('ajax-angular/angular-show/'+article.id, article).
					success(function(data){
						angCtrl.articles = data;
					}).
					error(function(){

					});
			}

			angCtrl.commentShow = function(){
				angCtrl.showRepForm = true;
			}
			angCtrl.commentHide = function(){
				angCtrl.showRepForm = false;
			}

			angCtrl.addComment = function(){
				$http.post('/ajax-angular/angular-comment/'+angCtrl.articles.id, angCtrl.newComment).
					success(function(data){
						angCtrl.showRepForm = false;
						angCtrl.comments.push(data);
						angCtrl.newComment = {
							body: '',
							forum_id: '',
							forum_title: '',
							user_name: '',
							user_id: window._laravel_token,
							created_at: ''
						};
					});
			}

			angCtrl.editComment = function(comment){
				comment.editMode = true;
				angCtrl.body = comment.body;
				angCtrl.updated_at = comment.updated_at;
			}

			angCtrl.saveComment = function(comment){
				$http.put('ajax-angular/comment-update/'+comment.id, comment).
				success(function(data){
					comment.updated_at = data.updated_at;
					comment.editMode = false;
				})
			}

			angCtrl.cancelCommentEdit = function(comment){
				comment.body = angCtrl.body;
				comment.editMode = false;
			}

			angCtrl.editArticle = function(article){
				article.editMode = true;
				angCtrl.title = article.title;
				angCtrl.body = article.body;
				angCtrl.updated_at = article.updated_at;
			}
			angCtrl.saveArticle = function(article){
					$http.put('ajax-angular/angular-update/'+article.id, article).
					success(function(data){
						article.editMode = false;
						article.updated_at = data.updated_at;
					})
			}
			angCtrl.cancel = function(article){
				article.title = angCtrl.title;
				article.body = angCtrl.body;
				article.editMode = false;
			}

			angCtrl.search = function(value, index) {
			return value.result == null;
			}

			angCtrl.showPopover = function() {
			  angCtrl.popoverIsVisible = true; 
			};

			angCtrl.hidePopover = function () {
			  angCtrl.popoverIsVisible = false;
			};

			angCtrl.addVesti = function() {
				$http.post('/ajax-angular/angular-vesti/', angCtrl.newVesti).
					success(function(data){
						angCtrl.vesti.push(data);
						angCtrl.newVesti = {
							title:'',
							body: '',
							user_name: '',
							user_id: window._laravel_token,
							created_at: ''
						};
					});
			}

			angCtrl.showVest = function(vest) {
				angCtrl.hideVestiTable = true;
				$http.get('ajax-angular/angular-show-vesti/'+vest.id, vest).
					success(function(data){
						angCtrl.vest = data;
					}).
					error(function(){

					});
			}
	})