<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Forum;
use App\RoleUser;


class DatabaseSeeder extends Seeder {

	protected $tables = ['users', 'forum', 'roles', 'role_user', 'vesti', 'comments'];
	protected $seeders = ['UserSeeder', 'ForumSeeder', 'RoleSeeder', 'RoleUserSeeder', 'VestiSeeder', 'CommentSeeder'];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	  {
	    Eloquent::unguard();

	    $this->cleanDatabase();

	    foreach($this->seeders as $seedClass)
	    {
	      $this->call($seedClass);
	    }
	  } // end run() fn

	  private function cleanDatabase()
	  {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0');

	    foreach($this->tables as $table)
	    {
	      DB::table($table)->truncate();
	    }

	    DB::statement('SET FOREIGN_KEY_CHECKS=1');
	  } // end cleanDatabase() fn

}
