<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RoleUser;

class RoleUserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
				RoleUser::create([ 
					'user_id' => 1, 
					'role_id' => 1 
				]);

	}

}