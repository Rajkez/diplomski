<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Forum;
use App\Comment;

class CommentSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
				Comment::create([ 
					'body' => 'Ovo je test komentar ubacen za temu "Test"',
					'forum_id'=>'1', 
					'forum_title' => 'Test',
					'user_name' => 'admin',
					'user_id' => '1'
				]);

	}

}