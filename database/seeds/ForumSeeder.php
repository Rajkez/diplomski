<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Forum;

class ForumSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
				Forum::create([ 
					'title' => 'Test', 
					'body' => 'Lorem ipsum dolor sit amet, in quidam concludaturque eam, usu an alii hinc malorum, quo cibo erat consulatu ut. Ius fastidii intellegam definitionem et, mei ipsum deterruisset consequuntur eu, nec ad facete lucilius. Ne vocibus convenire necessitatibus mel, minim salutandi an vim. Mucius epicurei mea ei, quis movet fabulas eum te, eam cu dicat saepe accusamus. Prima inani et vim.', 
					'user_name'=>'admin', 
					'user_id' => '1'
				]);

	}

}