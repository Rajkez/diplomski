<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Vesti;

class VestiSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
				Vesti::create([ 
					'title' => 'Upravljanje znanjem', 
					'body' => ' Znanje je informacija koja menja nešto ili nekoga - ili tako što postaje osnova za neko delovanje, ili, pak, čineći osobu (ili organizaciju) sposobnom za drugačije ili efikasnije delovanje.
								Pod pojmom upravljanje znanjem podrazumeva se proces kreiranja, prikupljanja, i korištenja znanja u cilju unapređenja poslovnih performansi preduzeća. Upravljanje znanjem podrazumeva upravljanje znanjima zaposlenih, ali i kupaca, dobavljača, distributera, kooperanata, i drugih poslovnih partnera - poslovnih banaka, advokatskih kancelarija, i svih ostalih koji su na bilo koji način umešani u poslovne procese firme koji rezultuju realizacijom nove vrednosti za kupca. Postindustrijski razvoj definiše intelektualni kapital kao najveću vrednost u bilansu preduzeća. Naša knjigovodstvena praksa još ne vrednuje na pravi način ovaj faktor, ali u tržišnoj bitci njegov značaj je veći nego značaj bilo koje druge bilansne pozicije. Na tržištu će biti uspešnije one firme koje mogu tržisnom izazovu suprotstaviti više znanja, to znanje bolje distribuirati među zaposlenima, i distribuirano znanje bolje upotrebiti u kreiranju odgovora na izazov.
								U industrijskom načinu poslovanja uspesno predviđanje je ključ kompetentnosti firme. Predviđanje kakav treba da bude novi proizvod da bi bio prihvaćen na tržistu, kakva će biti tržišna moć kupaca, kakvi će biti trendovi prodaje, kakav će uticaj na poslovanje imati zakonska regulativa, itd..., su osnov za postavljanje strategije firme. U dobu znanja, firme postaju mnogo fleksibilnije i stiču sposobnost da proizvode u skladu sa potrebama i željama konkretnog, poznatog kupca. Sistemi za upravljanje znanjem omogućuju firmi da odgovori uspešno na svaki izazov. Ovo je neophodno, jer smo u dobu sve bržih i sve učestalijih promena. Promene se danas dešavaju takvom brzinom da je uspešno predviđanje sve teže i teže, a često i nemoguće. U ovakvim, turbulentnim uslovima, sposobnost prihvatanja iznenađenja je od esencijalnog značaja za opstanak i napredovanje firme.
								Dok je u industrijskom dobu osnov dobrog poslovanja bila hijerarhija, subordinacija i pokornost, danas se oni odbacuju, jer su osnov za rigidnost, nefleksibilnost firme, osnov za neosetljivost za želje i potrebe kupaca i nesenzitivnost za spoljašnje i unutrašnje izazove. Način za povećanje sposobnosti firme da udovolji željama i potrebama kupaca (i tako ostvari konkurentsku prednost), da firma stekne toliko potrebnu fleksibilnost da može da prihvata iznenadjenja, preživi ih i pri tom ojača, je ovlašćivanje zaposlenih.
								Ovlašćivanje (empowerment) podrazumeva predavanje menadžerskih ovlašćenja i odgovornosti svakom zaposlenom, ali, naravno, i njegovu prethodnu osposobljenost i motivisanost da ta ovlašćenja na adekvatan način koristi. Svaki zaposleni sada postaje radnik znanja, i odgovoran za posao koji obavlja. Svaki zaposleni je deo tima, i odgovoran je za ceo proces u kojem kao član tima učestvuje, a ne samo za svoj posao u okviru procesa! Firme danas ne mogu opstati na tržistu i izdržati konkurentsku trku ako nisu u stanju da stvaraju nova znanja koja će odmah i primenjivati, u cilju poboljšanja svojih poslovnih performansi.'
				]);

	}

}