<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vesti extends Model {

	protected $table = 'vesti';

	protected $fillable = ['title', 'body'];

}
