<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Forum;
use App\Comment;
use App\User;
use App\Role;
use App\Vesti;
Use Input;

use Request;

//use Illuminate\Http\Request; has to be replaced with use Request;

class UzController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('article.index',['articles'=>Auth::user()->articles, 'user'=>Auth::user()]); //->ispisuje sve artikle user-a
	}

	public function indexCom()
	{
		return view('indexCom',['comments'=>Auth::user()->comments, 'user'=>Auth::user()]); //->ispisuje sve artikle user-a
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('article.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$article = Forum::find($id);
		return view('show', ['article' => $article]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$article = Forum::find($id);

		return view('article.edit', ['article' => $article]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$inputs = Request::all();
        $inputs['user_id'] = Auth::user()->id;
        $article =  Forum::find($id);
       	$article->update($inputs);
       
       	return redirect()->route('article.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$article = Forum::find($id);

		$article->delete();

		return redirect()->route('article.index');
	}


	function ajaxArticles()
	{
		return view('ajax-articles');
	}

	function ajaxComments()
	{
		return view('ajax-comments');
	}

	function ajaxVesti()
	{
		return view('ajax-vesti');
	}

	function ajaxArticlesUpdate($id)
	{
		$inputs = Input::all();
		if(Request::ajax()){
			$article = Forum::find($id);
			$article->update($inputs);
		}
		return $article;
	}

	function ajaxArticlesStore()
	{
		$inputs = Input::all();

		if(Request::ajax()){

			$inputs['user_id'] = Auth::user()->id;
        	$article = Forum::create($inputs);
		}
		return $article;
	}

	function ajaxArticlesDelete($id)
	{
		$article = Forum::findOrFail($id);

		if(Request::ajax()){

			$article->delete();

		}
		return $id;
	}

	public function udruzenje()
	{
		return view('udruzenje');
	}

	public function forum()
	{
		return view('forum');
	}

	public function kontakt()
	{
		return view('kontakt');
	}

	public function seminari()
	{
		return view('seminari');
	}

	public function buducnost()
	{
		return view('buducnost');
	}

	public function nalog()
	{
		return view('nalog');
	}

	function ispisi()
	{
		return Forum::all();
	}

	function ispisiCom()
	{
		return Comment::all();
	}

	function ispisiVesti()
	{
		return Vesti::all();
	}


	function ajaxAngularStore(){
		$inputs = Input::all();
		$inputs['user_id'] = Auth::user()->id;
		$inputs['user_name'] = Auth::user()->name;
        $article = Forum::create($inputs);

		return $article;
	}

	function ajaxAngularComment($id){
		$inputs = Input::all();
		$inputs['user_id'] = Auth::user()->id;
		$inputs['user_name'] = Auth::user()->name;
		$inputs['forum_id'] = Forum::findOrFail($id)->id;
		$inputs['forum_title'] = Forum::findOrFail($id)->title;
        $comment = Comment::create($inputs);

		return $comment;
	}

	function ajaxAngularCommentUpdate($id){
		$inputs = Input::all();
		$comment = Comment::findOrFail($id);
		$comment->update($inputs);

		return $comment;
	}

	function ajaxAngularCommentDelete($id){

		$comment = Comment::findOrFail($id);
		$comment->delete();

		return $comment;
	}

	function ajaxAngularDelete($id){
		$article = Forum::findOrFail($id);
		$article->delete();

		return $article;
	}

	function ajaxAngularUpdate($id){

		$inputs = Input::all();
		$article = Forum::findOrFail($id);
		$article->update($inputs);
		return $article;
	}

	function ajaxAngularShow($id){

		$article = Forum::findOrFail($id);
		return $article;
	}
	function ajaxAngularShowVesti($id){

		$vest = Vesti::findOrFail($id);
		return $vest;
	}

	function ajaxAngularVesti(){
		$inputs = Input::all();
		$inputs['user_id'] = Auth::user()->id;
		$inputs['user_name'] = Auth::user()->name;
        $vesti = Vesti::create($inputs);

		return $vesti;
	}

	function ajaxAngularVestDelete($id){

		$vesti = Vesti::findOrFail($id);
		$vesti->delete();

		return $vesti;
	}
}
