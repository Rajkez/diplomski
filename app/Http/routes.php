<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::resource('article', 'UzController');

Route::resource('indexCom', 'UzController@indexCom');
Route::get('udruzenje', 'UzController@udruzenje');

// PROTECTED
Route::group(array('before' => 'auth'), function()
{ 
	Route::get('forum', array('as' => 'forum', 'uses' => 'UzController@forum'));
});
// AUTH FILTER
Route::filter('auth', function()
{
    if (Auth::guest()) return Redirect::to('auth/login');
});

Route::get('kontakt', 'UzController@kontakt');
Route::get('seminari', 'UzController@seminari');
Route::get('buducnost', 'UzController@buducnost');
Route::get('nalog', 'UzController@nalog');

//ruta za json 
Route::get('ispis', 'UzController@ispisi');
Route::get('ispisCom', 'UzController@ispisiCom');
Route::get('ispisVesti', 'UzController@ispisiVesti');

//ruta za upisivanje podataka
Route::post('ajax-angular/angular-store', 'UzController@ajaxAngularStore');
//ruta za upisivanje komentara
Route::post('ajax-angular/angular-comment/{id}', 'UzController@ajaxAngularComment');
//ruta za upisivanje vesti
Route::post('ajax-angular/angular-vesti/', 'UzController@ajaxAngularVesti');
//ruta za brisanje podataka
Route::delete('ajax-angular/{id}/delete', 'UzController@ajaxAngularDelete');
//ruta za menjanje podataka
Route::put('ajax-angular/angular-update/{id}', 'UzController@ajaxAngularUpdate');
//ruta za ispis tema
Route::get('ajax-angular/angular-show/{id}', 'UzController@ajaxAngularShow');
//rute za obradu komentara
Route::put('ajax-angular/comment-update/{id}', 'UzController@ajaxAngularCommentUpdate');
//ruta za brisanje komentara
Route::delete('ajax-angular/angular-comment/{id}/delete', 'UzController@ajaxAngularCommentDelete');
//ruta za ispis vesti
Route::get('ajax-angular/angular-show-vesti/{id}', 'UzController@ajaxAngularShowVesti');
//ruta za brisanje vesti
Route::delete('ajax-angular/angular-vest/{id}/delete', 'UzController@ajaxAngularVestDelete');


Route::get('ajax-articles', 'UzController@ajaxArticles');
Route::get('ajax-comments', 'UzController@ajaxComments');
Route::get('ajax-vesti', 'UzController@ajaxVesti');
Route::put('ajax-articles/{id}', 'UzController@ajaxArticlesUpdate');
Route::post('ajax-articles/ajax-store', 'UzController@ajaxArticlesStore');
Route::delete('ajax-articles/{id}/delete', 'UzController@ajaxArticlesDelete');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
