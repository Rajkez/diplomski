<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model {


	protected $table = 'forum';

	protected $fillable = ['title', 'body', 'user_name', 'user_id'];

	public function user(){

		return $this->belongsTo('App\User');
	}

	public function comments(){

		return $this->hasMany('App\Comment');
	}
}
