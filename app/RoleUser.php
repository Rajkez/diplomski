<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model {

	protected $table = 'role_user';

	/**
     * Set timestamps off
     */
    public $timestamps = false;
 
    /**
     * Get users with a certain role
     */

}
